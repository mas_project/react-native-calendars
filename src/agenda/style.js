import EStyleSheet from 'react-native-extended-stylesheet';
import * as defaultStyle from '../style';
import platformStyles from './platform-style';
import { HEADER_HEIGHT, KNOB_HEIGHT } from './index';
import { getRemValue } from '../../../../helpers/styleHelper';

const STYLESHEET_ID = 'stylesheet.agenda.main';

export default function styleConstructor(theme = {}) {
    const appStyle = { ...defaultStyle, ...theme };
    const { knob, weekdays, knobClose } = platformStyles(appStyle);
    return EStyleSheet.create({
        knob,
        knobClose,
        weekdays,
        weekdaysWeekMode: {
            ...platformStyles(appStyle).weekdays,
            paddingLeft: getRemValue(65),
        },
        header: {
            overflow: 'hidden',
            justifyContent: 'flex-end',
            position: 'absolute',
            height: '100%',
            width: '100%',
        },
        calendar: {
            flex: 1,
            borderBottomWidth: 1,
            borderColor: appStyle.separatorColor,
        },
        knobContainer: {
            flex: 1,
            position: 'absolute',
            left: 0,
            right: 0,
            height: getRemValue(KNOB_HEIGHT),
            bottom: -10,
            alignItems: 'center',
            backgroundColor: appStyle.calendarBackground,
        },
        knobCloseContainer: {
            flex: 1,
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0,
            height: getRemValue(KNOB_HEIGHT),
            alignItems: 'center',
        },
        weekday: {
            width: getRemValue(32),
            textAlign: 'center',
            fontSize: getRemValue(13),
            color: appStyle.textSectionTitleColor,
        },
        weekdayWeekMode: {
            width: getRemValue(24),
            textAlign: 'center',
            fontSize: getRemValue(13),
            color: appStyle.textSectionTitleColor,
        },
        reservations: {
            flex: 1,
            marginTop: HEADER_HEIGHT,
            backgroundColor: appStyle.backgroundColor,
        },
        ...(theme[STYLESHEET_ID] || {}),
    });
}
