import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { xdateToData } from '../../interface';
import XDate from 'xdate';
import dateutils from '../../dateutils';
import styleConstructor from './style';

class ReservationListItem extends Component {
    constructor(props) {
        super(props);
        this.styles = styleConstructor(props.theme);
    }

    shouldComponentUpdate(nextProps) {
        const idx1 = this.props.calendarModeIdx;
        const idx2 = nextProps.calendarModeIdx;
        const isModeChanged = idx1 !== idx2;

        if (idx1 === 1) {
            const w1 = this.props.selectedDay.getWeek();
            const w2 = nextProps.selectedDay.getWeek();
            const isWeekChanged = this.props.weekHasChanged(w1, w2);

            const d1 = this.props.data;
            const d2 = nextProps.data;
            const isDataChanged = JSON.stringify(d1) !== JSON.stringify(d2);

            const { dayHasPressed } = nextProps;

            if (isWeekChanged || isModeChanged || isDataChanged) {
                return isWeekChanged || isModeChanged || isDataChanged;
            } else if (dayHasPressed) {
                this.props.callbackHandleDayPressed();
                return dayHasPressed;
            } else {
                return false;
            }

            return isWeekChanged || isModeChanged || isDataChanged || dayHasPressed;
        } else {
            const r1 = this.props.item;
            const r2 = nextProps.item;
            let isRowChanged = true;

            if (!r1 && !r2) {
                isRowChanged = false;
            } else if (r1 && r2) {
                if (r1.day.getTime() !== r2.day.getTime()) {
                    isRowChanged = true;
                } else if (!r1.reservation && !r2.reservation) {
                    isRowChanged = false;
                } else if (r1.reservation && r2.reservation) {
                    if ((!r1.date && !r2.date) || (r1.date && r2.date)) {
                        isRowChanged = this.props.rowHasChanged(r1.reservation, r2.reservation);
                    }
                }
            }

            return isModeChanged || isRowChanged;
        }
    }

    renderDate(date, item) {
        if (this.props.renderDay) {
            return this.props.renderDay(date ? xdateToData(date) : undefined, item);
        }
        const today = dateutils.sameDate(date, XDate()) ? this.styles.today : undefined;
        if (date) {
            return (
                <View style={this.styles.day}>
                    <Text allowFontScaling={false} style={[this.styles.dayNum, today]}>
                        {date.getDate()}
                    </Text>
                    <Text allowFontScaling={false} style={[this.styles.dayText, today]}>
                        {XDate.locales[XDate.defaultLocale].dayNamesShort[date.getDay()]}
                    </Text>
                </View>
            );
        } else {
            return <View style={this.styles.day} />;
        }
    }

    render() {
        const { reservation, date } = this.props.item;
        let content;
        if (reservation) {
            const firstItem = date ? true : false;
            content = this.props.renderItem(reservation, firstItem);
        } else {
            content = this.props.renderEmptyDate(date);
        }
        return (
            <View style={this.styles.container}>
                {this.renderDate(date, reservation)}
                <View style={{ flex: 1 }}>{content}</View>
            </View>
        );
    }
}

export default ReservationListItem;
